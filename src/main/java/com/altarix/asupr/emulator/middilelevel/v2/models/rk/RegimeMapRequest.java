package com.altarix.asupr.emulator.middilelevel.v2.models.rk;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.List;

/**
 * Create by Anton Arefyev
 * arefevayu@altarix.ru
 */
@Data
@Builder
public class RegimeMapRequest implements Serializable {

    @Builder.Default
    @XmlElement(name = "RK", required = true, nillable = false, type = RegimeMap.class)
    @JsonProperty(value = "RK", required = true)
    private List<RegimeMap> rkList;
}

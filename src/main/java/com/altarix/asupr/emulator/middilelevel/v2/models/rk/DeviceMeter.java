package com.altarix.asupr.emulator.middilelevel.v2.models.rk;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.List;

/**
 * Create by Anton Arefyev
 * arefevayu@altarix.ru
 */
@Data
@Builder
public class DeviceMeter implements Serializable {

    @XmlAttribute(name="SerialNumber", required = true)
    @JsonProperty(value = "SerialNumber", required = true)
    private String serialNumber;
    @Builder.Default
    @XmlElement(name = "subsystem", required = true, nillable = false, type = Subsystem.class)
    @JsonProperty(value = "subsystem", required = true)
    private List<Subsystem> subsystemList;

}

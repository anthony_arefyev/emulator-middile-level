package com.altarix.asupr.emulator.middilelevel.v2.models.rk;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.List;

/**
 * Create by Anton Arefyev
 * arefevayu@altarix.ru
 */
@Data
@Builder
public class Subsystem implements Serializable {

    @XmlAttribute(name="ResType", required = true)
    @JsonProperty(value = "ResType", required = true)
    private String resType;

    @XmlAttribute(name="Version", required = true)
    @JsonProperty(value = "Version", required = true)
    private String version;

    @XmlAttribute(name="Season", required = true)
    @JsonProperty(value = "Season", required = true)
    private String season;

    @Builder.Default
    @XmlElement(name = "params", required = true, nillable = false, type = Param.class)
    @JsonProperty(value = "params", required = true)
    private List<Param> params;

}

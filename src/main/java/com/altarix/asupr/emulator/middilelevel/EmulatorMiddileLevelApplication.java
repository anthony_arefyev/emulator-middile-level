package com.altarix.asupr.emulator.middilelevel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmulatorMiddileLevelApplication {

    public static void main(String[] args) {

        SpringApplication.run(EmulatorMiddileLevelApplication.class, args);
        System.out.println("start url: http://localhost:8080/");
    }

}

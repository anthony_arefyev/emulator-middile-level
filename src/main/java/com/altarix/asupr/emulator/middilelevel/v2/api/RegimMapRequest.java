package com.altarix.asupr.emulator.middilelevel.v2.api;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Create by Anton Arefyev
 * arefevayu@altarix.ru
 */
@Data
@Builder
public class RegimMapRequest implements Serializable {
//
//    private static final long serialVersionUID = -5327155697140385204L;
//
//    @Builder.Default
//    @XmlElement(name = "RK", required = true, nillable = false, type = RegimeMap.class)
//    @JsonProperty(value = "RK", required = true)
//    private List<RegimeMap> rkList;
//
//    @Data
////    @Builder
//    public class RegimeMap {
//
//        @XmlAttribute(name="uspd_id", required = true)
//        @JsonProperty(value = "uspd_id", required = true)
//        private String uspd_id;
//
//        @Builder.Default
//        @XmlElement(name = "pu", required = true, nillable = false, type = DeviceMeter.class)
//        @JsonProperty(value = "pu", required = true)
//        private List<DeviceMeter> puList;
//
//        @Data
////        @Builder
//        public class DeviceMeter {
//
//            @XmlAttribute(name="SerialNumber", required = true)
//            @JsonProperty(value = "SerialNumber", required = true)
//            private String serialNumber;
//
//            @Builder.Default
//            @XmlElement(name = "subsystem", required = true, nillable = false, type = Subsystem.class)
//            @JsonProperty(value = "subsystem", required = true)
//            private List<Subsystem> subsystemList;
//
//            @Data
////            @Builder
//            public class Subsystem {
//
//                @XmlAttribute(name="ResType", required = true)
//                @JsonProperty(value = "ResType", required = true)
//                private String resType;
//
//                @XmlAttribute(name="Version", required = true)
//                @JsonProperty(value = "Version", required = true)
//                private String version;
//
//                @XmlAttribute(name="Season", required = true)
//                @JsonProperty(value = "Season", required = true)
//                private String season;
//
//                @Builder.Default
//                @XmlElement(name = "params", required = true, nillable = false, type = Param.class)
//                @JsonProperty(value = "params", required = true)
//                private List<Param> params;
//
//                @Data
////                @Builder
//                public class Param {
//
//                    private Map<String, ParamValue> paramsValue;
//
//                    @JsonAnyGetter
//                    public Map<String, ParamValue> getParamsValue() {
//                        return paramsValue;
//                    }
//
//                    @Data
////                    @Builder
//                    public class ParamValue {
//
//                        @JsonProperty(value = "AСС_UP", required = false)
//                        private Integer acc_up;
//                        @JsonProperty(value = "ACC_LOW", required = false)
//                        private Integer acc_low;
//                        @JsonProperty(value = "TECH_UP", required = false)
//                        private Integer tech_up;
//                        @JsonProperty(value = "TECH_LOW", required = false)
//                        private Integer tech_low;
//                    }
//                }
//            }
//        }
//    }
}

package com.altarix.asupr.emulator.middilelevel.v2.api;

import com.altarix.asupr.emulator.middilelevel.v2.models.rk.*;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Create by Anton Arefyev
 * arefevayu@altarix.ru
 */
@RestController
public class RegimeMapWebService {

    @GetMapping(name = "/rk")
    public RegimeMapRequest getRk(){

        List<RegimeMap> rkList = new ArrayList<>();
        List<DeviceMeter> puList = new ArrayList<>();
        List<Subsystem> subsystems = new ArrayList<>();
        List<Param> params = new ArrayList<>();
        Map<String, ParamValue> map = new HashMap<>();
        map.put("T1",
                ParamValue.builder()
                .acc_up(100).acc_low(80).tech_up(98).tech_low(82)
            .build()
        );
        map.put("A20",
                ParamValue.builder()
                .acc_up(5).acc_low(3).tech_up(4).tech_low(2)
            .build()
        );
        params.add(Param.builder()
                .paramsValue(map)
                .build()
        );
        subsystems.add(
                Subsystem.builder()
                .resType("ГВС2")
                .version("ГВС-Школа-в2")
                .season("Лето")
                    .params(params)
            .build()
        );
        puList.add(
                DeviceMeter.builder()
                .serialNumber("385214")
                .subsystemList(subsystems)
            .build()
        );
        rkList.add(
                RegimeMap.builder()
                .uspd_id("12345")
                .puList(puList)
            .build()
        );
        return RegimeMapRequest.builder()
                .rkList(rkList)
                .build();
    }

    @PostMapping(name = "/rk")
    @ResponseStatus(HttpStatus.OK)
    public void rk(@RequestBody RegimeMapRequest request){

        System.out.print(request.getRkList().get(0).getUspd_id());
        System.out.print(request.toString());
    }
}

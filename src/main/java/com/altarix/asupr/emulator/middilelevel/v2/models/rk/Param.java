package com.altarix.asupr.emulator.middilelevel.v2.models.rk;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * Create by Anton Arefyev
 * arefevayu@altarix.ru
 */
@Data
@Builder
public class Param implements Serializable {

    private static final long serialVersionUID = -5630016925156612849L;

    private Map<String, ParamValue> paramsValue;

    @JsonAnyGetter
    public Map<String, ParamValue> getParamsValue() {
        return paramsValue;
    }
}

package com.altarix.asupr.emulator.middilelevel.v2.models.rk;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.List;

/**
 * Create by Anton Arefyev
 * arefevayu@altarix.ru
 */
@Data
@Builder
public class RegimeMap implements Serializable {

    @XmlAttribute(name="uspd_id", required = true)
    @JsonProperty(value = "uspd_id", required = true)
    private String uspd_id;
    @Builder.Default
    @XmlElement(name = "pu", required = true, nillable = false, type = DeviceMeter.class)
    @JsonProperty(value = "pu", required = true)
    private List<DeviceMeter> puList;

}

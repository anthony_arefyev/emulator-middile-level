package com.altarix.asupr.emulator.middilelevel.v2.models.rk;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * Create by Anton Arefyev
 * arefevayu@altarix.ru
 */
@Data
@Builder
public class ParamValue implements Serializable {

    @JsonProperty(value = "AСС_UP", required = false)
    private Integer acc_up;
    @JsonProperty(value = "ACC_LOW", required = false)
    private Integer acc_low;
    @JsonProperty(value = "TECH_UP", required = false)
    private Integer tech_up;
    @JsonProperty(value = "TECH_LOW", required = false)
    private Integer tech_low;
}
